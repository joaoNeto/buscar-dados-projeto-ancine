const express = require('express');
const cheerio = require('cheerio');
const axios = require('axios');
var cors = require('cors');
const app = express();

var http = axios.create({
  baseURL: '',
  timeout: 1000,
});

let url = '';

app.use(cors());
app.get('/', async (req, res) => {
    if(typeof req.query.salic == 'undefined'){
        res.send({
            status: false, 
            msgDetails: 'Voce precisa informar o salic que deseja pesquisar os dados!',
            code: 008
        });
    }
    url = 'http://sad.ancine.gov.br/projetosaudiovisuais/ConsultaProjetosAudiovisuais.do?method=detalharProjeto&numSalic='+req.query.salic;
    res.send(await main());
});
app.listen(3001);

async function main(){
    let resultado = {};
    const response = await http.get(url);

    if(response.status == 200){

        let arrLinksGoogleValidos  = [];
        let $ = cheerio.load(response.data, { decodeEntities: true });

        var arrDadosPrincipal = [];
        var arrDadosFonteFinanciamento = [];
        $("table:nth-child(3) tr td:nth-child(2)").each(function(line){
            arrDadosPrincipal.push($(this).html());
        });

        $("table:nth-child(3) table:last-child tr:not('.titulo3')").each(function(i){
            if($(this).find('td:first-child').html() != '<strong>TOTAL</strong>')
                arrDadosFonteFinanciamento.push({
                    'nome' : $(this).find('td:first-child').html(),
                    'valorAprovado' : $(this).find('td:nth-child(2)').html(),
                    'valorCaptado'  : $(this).find('td:nth-child(3)').html(),
                    'saldo' : $(this).find('td:nth-child(4)').html(),
                });
        });

        let data = {
            'projeto': arrDadosPrincipal[1].replace('<strong>','').replace('</strong>',''),
            'salic': arrDadosPrincipal[2],
            'processo': arrDadosPrincipal[3],
            'responsavel': arrDadosPrincipal[4],
            'uf': arrDadosPrincipal[5],
            'diretor': arrDadosPrincipal[6],
            'produtor': arrDadosPrincipal[7],
            'roteirista': arrDadosPrincipal[8],
            'segmento': arrDadosPrincipal[9],
            'genero': arrDadosPrincipal[10],
            'formato': arrDadosPrincipal[11],
            'sinopse': arrDadosPrincipal[12],
            'situação': arrDadosPrincipal[13],
            'fontesFinanciamento': arrDadosFonteFinanciamento
        };
        // devolver mensagem padrao de salic nao encontrado caso nao tenha encontrado nenhum resultado dos sites
        resultado = {
            status: true, 
            msgDetails: 'Dados do projeto incentivado',
            data: data
        };
    } else {
        resultado = {
            status: false, 
            msgDetails: 'Erro ao acessar dados do google',
            code: 006
        };
    }
    return resultado;
}


function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}
